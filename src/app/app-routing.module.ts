import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DropFilesComponent } from './drop-files/drop-files.component';
import { GenerarDescargaComponent } from './generar-descarga/generar-descarga.component';
import { InicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';
import { MisArchivosComponent } from './mis-archivos/mis-archivos.component';
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [
  { path: '', redirectTo: '/inicio', pathMatch: 'full' },
  { path: 'inicio', component: InicioComponent },
  { path: 'mis-archivos', component: MisArchivosComponent },
  { path: 'login', component: LoginComponent },
  { path: 'drop-files', component: DropFilesComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'generar-descarga', component: GenerarDescargaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
