import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { MisArchivosComponent } from './mis-archivos/mis-archivos.component';
import { LoginComponent } from './login/login.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { DropFilesComponent } from './drop-files/drop-files.component';
import { RegistroComponent } from './registro/registro.component';
import { AuthInterceptorService } from './auth-interceptor.service';
import { GenerarDescargaComponent } from './generar-descarga/generar-descarga.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    MisArchivosComponent,
    LoginComponent,
    DropFilesComponent,
    RegistroComponent,
    GenerarDescargaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule,
    NgxFileDropModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
