import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { ArchivoControllerService, ArchivoWithRelations, UserControllerService } from '../openapi';
import { FileUploadControllerService } from '../openapi/api/fileUploadController.service';

@Component({
  selector: 'app-drop-files',
  templateUrl: './drop-files.component.html',
  styleUrls: ['./drop-files.component.css']
})
export class DropFilesComponent implements OnInit {



  public fileSelect: File | undefined;
  public urlDescarga: string | undefined;
  public isChecked: any;
  public restriccionAcceso: any;
  public archivos: ArchivoWithRelations[] | undefined;

  constructor(public loadService: FileUploadControllerService,
    public userController: UserControllerService,
    public archivosService: ArchivoControllerService) { }

  public tipo: '' | undefined;
  public msjValidacion: any

  ngOnInit(): void {
    this.usuario = sessionStorage.getItem('user') && (JSON.parse(sessionStorage.getItem('user') || "")) || null;
    this.tipo = this.usuario?.tipo;
    console.log(this.tipo);
    if (this.tipo && this.tipo != '') {
      this.archivosService.archivoControllerFind().subscribe(
        resArchivo => this.archivos = resArchivo
      )
    }

  }

  public files: NgxFileDropEntry[] = [];

  public mostrarmodal = false;
  public nuevacarpeta = false;
  public nuevodirectorio = false;
  public mostrarRestricciones = false;

  public archivo: any = {
    nombre: "",
    tamanio: 0,
    descargas: 0,
    version: '0',
    fecha: moment().format(),
    clave: undefined,
    caducidad: undefined,
    restriccion: '',
    url: '',
    directorio: 'home',
    vencimiento: false
  }

  public usuario: any;

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file
          console.log(droppedFile.relativePath, file);
          console.log(3);
          if (this.tipo && this.tipo != '') {
            if (this.tipo == 'free' && file.size / (1e+6) > 5) {
              this.msjValidacion = 'El archivo supera los 5MB para usuario FREE'
              return 
            } else {
              if (this.tipo == 'pro' && file.size / (1e+6) > 15) {
                this.msjValidacion = 'El archivo supera los 15MB para usuario PRO'
                return
              }
            }
          }
          this.msjValidacion = ''
          this.fileSelect = file

        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event: any) {
    console.log(event);
    console.log(1);
  }

  public fileLeave(event: any) {
    console.log(event);
    console.log(2);
  }

  modalcompartir() {
    this.mostrarmodal = true;
    console.log("modal compartir")
  }

  crearcarpeta() {
    this.nuevacarpeta = true;
    console.log("crear carpeta")

  }

  seleccionardirectorio() {
    this.nuevodirectorio = true;
    console.log("crear directorio")
  }

  restriccion() {
    this.restriccionAcceso = true;

  }

  directorios() {
    return new Set(this.archivos?.map(data => data.directorio))
  }

  sinRestriccion() {
    this.restriccionAcceso = false;
    this.archivo.restriccion = '';
    this.archivo.clave = undefined;
    this.archivo.caducidad = undefined
  }

  subirArchivo() {
    this.loadService.fileUploadControllerFileUpload(this.fileSelect).subscribe((dataArchivo: any) => {
      console.log(dataArchivo);
      if (this.archivo.caducidad != undefined) {
        this.archivo.caducidad = moment(this.archivo.caducidad).format()
      }
      let objectRequest = { ...this.archivo, url: "http://localhost:3000/files/" + dataArchivo.files[0].originalname, size: dataArchivo.files[0].size, nombre: dataArchivo.files[0].originalname }

      if (this.tipo && this.tipo != '') {
        this.archivosService.archivoControllerCreate(objectRequest).subscribe(data => {
          console.log('CREADO', data);
          //this.userfiles?.push(data);
          //this.idFile = data.id
          //this.urlDescarga = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/download-controller?id=" + this.idFile
          //this.idFile && this.archivosService.archivoControllerUpdateById(this.idFile, { url: fileObjetRequest.url + "/" + this.idFile }).subscribe(updateok => { console.log(updateok) })
          this.urlDescarga = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/generar-descarga?id=" + data.id;
          data.id && this.archivosService.archivoControllerUpdateById(data.id,
            { url: "http://localhost:3000/files/" + dataArchivo.files[0].originalname + "/" + data.id }).subscribe(updateok => { console.log(updateok) })
        });
      } else {
        this.archivosService.archivoControllerCreateSinUsuario(objectRequest).subscribe(
          data => {
            console.log(data)
            this.urlDescarga = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/generar-descarga?id=" + data.id;
            data.id && this.archivosService.archivoControllerUpdateById(data.id,
              { url: "http://localhost:3000/files/" + dataArchivo.files[0].originalname + "/" + data.id }).subscribe(updateok => { console.log(updateok) })

          }
        )

      }

    })


  }






}
