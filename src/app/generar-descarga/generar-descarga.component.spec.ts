import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarDescargaComponent } from './generar-descarga.component';

describe('GenerarDescargaComponent', () => {
  let component: GenerarDescargaComponent;
  let fixture: ComponentFixture<GenerarDescargaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerarDescargaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GenerarDescargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
