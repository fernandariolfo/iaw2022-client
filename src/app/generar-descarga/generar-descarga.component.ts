import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ArchivoControllerService } from '../openapi';

@Component({
  selector: 'app-generar-descarga',
  templateUrl: './generar-descarga.component.html',
  styleUrls: ['./generar-descarga.component.css']
})
export class GenerarDescargaComponent implements OnInit {

  public id: any;
  public archi: any;
  public mensajes: string | undefined;

  public clave: any;
  public claveValida: boolean | undefined;
  public msjValidacion: string | undefined;

  constructor(public archivo: ArchivoControllerService, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.claveValida = false;
    this.msjValidacion = '';
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      this.id && this.archivo.archivoControllerFindById(this.id).subscribe(data => {
        this.archi = data;
        console.log(this.archi)
        if (data.restriccion == "caducidad") {
          let date1 = new Date(moment(data.caducidad).format('YYYY-MM-DD'));
          let date2 = new Date(moment().format('YYYY-MM-DD'));
          if (date1 < date2) {
            this.claveValida = false
            this.msjValidacion = "Archivo vencido para su descarga"
          }else{
            this.claveValida = true
          }
        }
      },err =>{
        this.msjValidacion = "Archivo no disponible"
      })
    });

  }

  validar() {
    console.log('validar')
    if (this.archi.clave == this.clave) {
      this.claveValida = true
      this.msjValidacion = ''
    } else {
      this.msjValidacion  = 'La clave ingresada no es válida'
      this.claveValida = false
    }

      
  }

}
