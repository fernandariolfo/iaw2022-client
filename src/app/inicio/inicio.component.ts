import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { User, UserControllerService } from '../openapi';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  public user: any = {
    email: '',
    password:''
  }

  constructor(
    public userController: UserControllerService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login(){
    console.log("login");
    this.userController.userControllerLogin(this.user).subscribe(
      data => {
        console.log(data);
        sessionStorage.setItem("user", JSON.stringify(data) || "")
        this.router.navigate(['/mis-archivos'])
      }
    )
  }



}
