import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ArchivoControllerService, ArchivoWithRelations } from '../openapi';

@Component({
  selector: 'app-mis-archivos',
  templateUrl: './mis-archivos.component.html',
  styleUrls: ['./mis-archivos.component.css']
})

export class MisArchivosComponent implements OnInit {


  public archivos: ArchivoWithRelations[] = [];

  public archivo: ArchivoWithRelations = {
    nombre: "",
    tamanio: 0,
    descargas: 0,
    version: '',
    fecha: '',
    vencimiento: false
  }
  public usuario: any;
  public win: any;

  constructor(public archivosService: ArchivoControllerService, public router: Router) { }

  nuevoArchivoForm = new FormGroup({
    nombre: new FormControl(''),
    tamanio: new FormControl(''),
    descargas: new FormControl(''),
    version: new FormControl(''),
    fecha: new FormControl(''),
    clave: new FormControl('')
  });

  ngOnInit(): void {
    this.archivosService.archivoControllerFind().subscribe(
      resArchivo => this.archivos = resArchivo
    )
    console.log("accediendo con usuario loggueado")
    this.usuario = sessionStorage.getItem('user') && (JSON.parse(sessionStorage.getItem('user') || "")) || null;
    this.win = window 
  }

  onSubmit() {
    let paramsRequest = {
      directorio: "/",
      caducidad: "2022-10-10T20:05:17.715Z",
      ... this.archivo
    }
    if (this.archivo.id){
      this.archivosService.archivoControllerUpdateById(this.archivo.id, paramsRequest).subscribe(
        res => console.log("editado")
      )
    }else {
      this.archivosService.archivoControllerCreate(paramsRequest).subscribe(
        res => this.archivos.push(res)
      )
    }
      
  }

  eliminarArchivo(arch: ArchivoWithRelations) {
    if (arch.id) {
      this.archivosService.archivoControllerDeleteById(arch.id).subscribe(
        res => console.log("eliminado") 
      )
    }
  }

  editarArchivo(arch: ArchivoWithRelations) {
    this.archivo = arch;
  }

  cerrarSesion() {
    sessionStorage.removeItem('user')
    this.router.navigate(['inicio']);
  }

  

}

