import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserControllerService } from '../openapi';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  constructor(public userController: UserControllerService,
    private router: Router) { }

  ngOnInit(): void {
  }

  public msjValidacion: any

  public registeruser: any = {
    username: '',
    email: '',
    password:'',
    passwordrep: '',
    tipo: 'free',
    emailVerified: true,
    additionalProp1: {
      tipo: 'free'
    }
  }

  registrarse(){
    console.log("registrarse");
    if (!this.registeruser.email.includes("@")) {
      this.msjValidacion = "El mail debe tener el formato correcto"
      return
    }
    if (this.registeruser.password.length < 8) {
      this.msjValidacion = "La contraseña debe tener al menos 8 caracteres"
      return
    }
    if (this.registeruser.password != this.registeruser.reppassword) {
      this.msjValidacion = "Las contraseñas no coinciden"
      return
    }
    this.userController.userControllerSignUp({
      ...this.registeruser, additionalProp1: {
        tipo: this.registeruser.tipo
      }
    }).subscribe(
      data => {
        console.log("registro ok", data)
        sessionStorage.setItem("user", JSON.stringify(data) || "")
        return
      }
    )
    this.userController.userControllerLogin(this.registeruser).subscribe(
      data => {
        console.log(data);
        sessionStorage.setItem("user", JSON.stringify(data) || "")
        this.router.navigate(['/mis-archivos'])
      }
    )
  }

}

